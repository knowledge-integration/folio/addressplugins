import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'react-flexbox-grid';
import { AddressTextField, requiredValidator } from '@k-int/address-utils';

const AddressFieldsGBR = ({ name, textFieldComponent }) => {
  return (
    <>
      <Row>
        <Col xs={4}>
          <AddressTextField
            name={name ? `${name}.premise` : 'premise'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.premise" />}
            component={textFieldComponent}
          />
        </Col>
        <Col xs={4}>
          <AddressTextField
            name={name ? `${name}.thoroughfare` : 'thoroughfare'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.thoroughfare" />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
        <Col xs={4}>
          <AddressTextField
            name={name ? `${name}.postalCodeOrTown` : 'postalCodeOrTown'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.postalCodeOrTown" />}
            component={textFieldComponent}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={4}>
          <AddressTextField
            name={name ? `${name}.locality` : 'locality'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.locality" />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
        <Col xs={4}>
          <AddressTextField
            name={name ? `${name}.administrativeArea` : 'administrativeArea'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.administrativeArea" />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
        <Col xs={4}>
          <AddressTextField
            name={name ? `${name}.postalCode` : 'postalCode'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.postalCode" />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <AddressTextField
            name={name ? `${name}.country` : 'country'}
            label={<FormattedMessage id="ui-address-plugin-british-isles.country" />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
      </Row>
    </>
  );
};

AddressFieldsGBR.propTypes = {
  name: PropTypes.string,
  savedAddress: PropTypes.shape({
    addressLabel: PropTypes.string,
    countryCode: PropTypes.string,
    id: PropTypes.string,
    lines: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      type: PropTypes.shape({
        value: PropTypes.string.isRequired
      }).isRequired,
      value: PropTypes.string.isRequired
    })),
  }),
  textFieldComponent: PropTypes.object
};

export default AddressFieldsGBR;
