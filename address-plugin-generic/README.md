
# address-plugin-generic

This is a plugin designed for use with `address-plugin-utils` as part of a general set of plugins. This plugin in particular acts as a fallback, with fields available for all address fields in the model. 
## Generic information
For the generic information about how these plugins operate, please see [here](https://gitlab.com/knowledge-integration/folio/addressplugins/-/blob/main/README.md). (I'd HIGHLY recommend starting there rather than trying to jump in blind here).
