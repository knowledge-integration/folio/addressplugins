import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'react-flexbox-grid';
import { AddressTextField, requiredValidator } from '@k-int/address-utils';

const AddressFieldsUSA = ({ country, name, textFieldComponent }) => {
  return (
    <>
      <Row>
        <Col xs={6}>
          <AddressTextField
            name={name ? `${name}.addressLineOne` : 'addressLineOne'}
            label={<FormattedMessage id={`ui-address-plugin-north-america.${country}.addressLineOne`} />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
        <Col xs={6}>
          <AddressTextField
            name={name ? `${name}.addressLineTwo` : 'addressLineTwo'}
            label={<FormattedMessage id={`ui-address-plugin-north-america.${country}.addressLineTwo`} />}
            component={textFieldComponent}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <AddressTextField
            name={name ? `${name}.locality` : 'locality'}
            label={<FormattedMessage id={`ui-address-plugin-north-america.${country}.locality`} />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <AddressTextField
            name={name ? `${name}.administrativeArea` : 'administrativeArea'}
            label={<FormattedMessage id={`ui-address-plugin-north-america.${country}.administrativeArea`} />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
        <Col xs={6}>
          <AddressTextField
            name={name ? `${name}.postalCode` : 'postalCode'}
            label={<FormattedMessage id={`ui-address-plugin-north-america.${country}.postalCode`} />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <AddressTextField
            name={name ? `${name}.country` : 'country'}
            label={<FormattedMessage id={`ui-address-plugin-north-america.${country}.country`} />}
            component={textFieldComponent}
            required
            validator={requiredValidator}
          />
        </Col>
      </Row>
    </>
  );
};

AddressFieldsUSA.propTypes = {
  country: PropTypes.string,
  intl: PropTypes.object.isRequired,
  name: PropTypes.string,
  savedAddress: PropTypes.shape({
    addressLabel: PropTypes.string,
    countryCode: PropTypes.string,
    id: PropTypes.string,
    lines: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      type: PropTypes.shape({
        value: PropTypes.string.isRequired
      }).isRequired,
      value: PropTypes.string.isRequired
    })),
  }),
  textFieldComponent: PropTypes.object
};

export default AddressFieldsUSA;
