import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

const AddressTextField = ({ validator, ...restOfProps }) => {
  return (
    <Field
      {...restOfProps}
      parse={v => v}
      validate={validator}
    />
  );
};

AddressTextField.propTypes = {
  validator: PropTypes.func,
};

export default AddressTextField;
