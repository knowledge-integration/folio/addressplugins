// This is essentially a clone of the validator found in https://github.com/folio-org/stripes-erm-components/blob/master/lib/validators.js
import { FormattedMessage } from 'react-intl';

const required = value => {
  const blankString = /^\s+$/;
  if ((value && !blankString.test(value)) || value === false || value === 0) {
    return undefined;
  }
  return <FormattedMessage id="stripes-core.label.missingRequiredField" />;
};

export { required as requiredValidator };
