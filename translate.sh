#!/bin/bash
path_to_translations="/home/ethan/FolioModules/addressplugins/$1/translations/ui-$1"

translate() {
  if [ -z "$2" ];
  then
    cat en.json | docker run -i translate $1 > "$path_to_translations/$1.json";
  else
    cat en.json | docker run -i translate $1 > "$path_to_translations/$2.json";
  fi
}

translateLanguages=(
  'ar'
  'ca'
  'cs:cs_CZ'
  'da'
  'de'
  'es'
  'fr'
  'he'
  'hi:hi_IN'
  'hu'
  'it:it_IT'
  'ja'
  'ko'
  'nb'
  'nn'
  'pl'
  'pt:pt_PT'
  'ru'
  'sv'
  'ur'
  'zh:zh_CN'
)

cd "$path_to_translations"

for i in "${translateLanguages[@]}"
do
  # delete previous array/list (this is crucial!)
  unset fileName
  # split sub-list if available
  if [[ $i == *":"* ]];
  then
    # split server name from sub-list
    tmpLangArr=(${i//:/ })
    i=${tmpLangArr[0]}
    fileName=${tmpLangArr[1]}
  fi
  if [ -z "$fileName" ];
  then
    translate $i
    if [ $? -ne 0 ];
    then
      echo "{}" > "$path_to_translations/$i.json";
    fi;
  else
    translate $i $fileName
    if [ $? -ne 0 ];
    then
      echo "{}" > "$path_to_translations/$fileName.json";
    fi;
  fi
done